import * as Dice from "./sheets/dice.js";

export const tor1eUtilities = {};

/**
 * Take an array of values and split it in 2 part depending on the index in the array
 * @param abilities
 * @returns {*[]}
 * @private
 */
function _splitInTwo(abilities) {
    let lefts = abilities.filter((v, i) => !(i % 2));
    let rights = abilities.filter((v, i) => i % 2);
    return [lefts, rights]
}


tor1eUtilities.filtering = {
    /**
     * Filter a list of items by its type (weapons, specialAbility, armour, ...) and its group ( for specialAbility for example, it could be Hate, Virtues, Valor)
     * This method returns left and right because it is used in a Two columns display.
     * @param items
     * @param type
     * @param subgroup
     * @returns {*[]}
     */
    "getAndSplitItemsBy": function (items, type, subgroup = null) {
        let elements = this.getItemsBy(items, type, subgroup);
        return _splitInTwo(elements);
    },
    /**
     * Get a list of elements from Items filtered by the Type (Armour, weapon, special ability, ...)
     * @param items List of all items
     * @param type The type of items you want to filter
     * @param subgroup The subgroup of type of Item you want to filter (optional)
     * @returns {*}
     */
    "getItemsBy": function (items, type, subgroup = null) {
        if (subgroup == null) {
            return items.filter(function (item) {
                return item.type === type;
            });
        } else {
            return items.filter(function (item) {
                return item.type === type && item.data.group.value === subgroup;
            });
        }
    },
    /**
     * Get a list of elements from Items filtered by the Type (Armour, weapon, special ability, ...)
     * @param items List of all items
     * @param type The type of items you want to filter
     * @param subgroup The subgroup of type of Item you want to filter (optional)
     * @returns {*}
     */
    "getItemsNot": function (items, type, subgroup) {
        return items.filter(function (item) {
            return item.type === type && item.data.group.value !== subgroup;
        });
    },
    /**
     * Get a list of elements from Items filtered by the Type (Armour, weapon, special ability, ...)
     * @param items List of all items
     * @param type The type of items you want to filter
     * @param subgroup The subgroup of type of Item you want to filter (optional)
     * @param equipped
     * @returns {*}
     */
    "getItemBy": function (items, type, subgroup = null, equipped = false) {
        if (subgroup == null) {
            return items.find(function (item) {
                if (equipped && item.data.equipped) {
                    return item.type === type && item.data.equipped.value === true;
                } else {
                    return item.type === type;
                }
            });
        } else {
            return items.find(function (item) {
                if (equipped && item.data.equipped) {
                    return item.type === type && item.data.group.value === subgroup && item.data.equipped.value === true;
                } else {
                    return item.type === type && item.data.group.value === subgroup;
                }
            });
        }
    },
    /**
     * Get a list of elements from Items filtered by the Type (Armour, weapon, special ability, ...)
     * The list of elements can be filtered by a list of groups. All the elements belonging to one of the groups.
     * @param items
     * @param type
     * @param groups
     * @param equipped
     * @returns {*}
     */
    "getItemIn": function (items, type, groups = null, equipped = false) {
        return items.find(function (item) {
            if (groups === null) {
                if (equipped && item.data.equipped) {
                    return item.type === type && item.data.equipped.value === true;
                } else {
                    return item.type === type;
                }
            } else {
                if (equipped && item.data.equipped) {
                    return item.type === type && groups.includes(item.data.group.value) && item.data.equipped.value === true
                } else {
                    return item.type === type && groups.includes(item.data.group.value)
                }
            }
        });
    }
}

/**
 * Get itemId from an event by bubbling to the item class and get the _id
 * @param event
 * @returns {string}
 * @private
 */
function _getItemId(event) {
    let element = event.currentTarget;
    return element.closest(".item").dataset.itemId;
}

function _getModifier(element, extra) {
    let actionBonusRaw = element.dataset.actionValueBonus;
    let actionBonus = (actionBonusRaw === undefined || actionBonusRaw === "") ? 0 : parseInt(actionBonusRaw);
    return (extra.favouredSkillValue && element.dataset.actionFavouredValue === "true")
        ? extra.favouredSkillValue + actionBonus
        : 0 + actionBonus;
}

tor1eUtilities.eventsProcessing = {
    /**
     * Roll some dice if you click on stats depending on the value of the stat
     * @param extra
     * @param event
     */
    "onStatName": function (extra = {}, event) {
        let element = event.currentTarget;
        let favouredBonus = _getModifier(element, extra);
        let actionBonusRaw = element.dataset.actionValueBonus;
        let actionBonus = actionBonusRaw === "" ? 0 : parseInt(actionBonusRaw);
        let actionDiceRaw = element.dataset.actionValueDice;
        let actionDice = actionDiceRaw === "" ? 0 : parseInt(actionDiceRaw);

        Dice.taskCheck({
            actor: this.actor,
            user: game.user,
            actionValue: actionDice,
            actionName: element.dataset.actionName,
            wearyRoll: this.actor.data.data.stateOfHealth.weary.value,
            modifier: favouredBonus + actionBonus,
            shadowServant: extra.shadowServant,
        });
    },

    /**
     * Toggle the visibility of the block show/hide
     * @param event
     * @returns {*}
     */
    "onToggle": async function (event) {
        event.preventDefault();
        let element = event.currentTarget;
        $(element).siblings(".editor-container").toggle();
        $(element).toggleClass("expanded");
    },

    /**
     * Toggle the visibility of the block show/hide
     * @param event
     * @returns {*}
     */
    "onEditorToggle": async function (event) {
        event.preventDefault();
        let element = event.currentTarget;
        let editorWrapper = $(element).closest('li').children('.editor-container-toggelable');
        $(editorWrapper).toggleClass("show");
        $(editorWrapper).find(".editor-content").show();
        $(editorWrapper).find(".tox-tinymce").hide();
    },

    /**
     * Edit an item when you click on the edit icon
     * using a Popup window.
     * @param event
     * @returns {*}
     */
    "onItemEdit": async function (event) {
        event.preventDefault();
        let itemId = _getItemId(event);
        let item = this.actor.getOwnedItem(itemId);
        item.sheet.render(true);
    },

    /**
     * Delete an item when you click on the trash
     * @param event
     * @returns {*}
     */
    "onItemDelete": function (event) {
        event.preventDefault();
        if (event.altKey) {
            let itemId = _getItemId(event);
            return this.actor.deleteOwnedItem(itemId);
        }
    },

    /**
     * Update the value of a stat when you change the value in the input text box.
     * Should be used for example when an item is in a sheet because the object doesn't belong to the proper stat of the sheet.
     * @param extra
     * @param event
     * @returns {*}
     */
    "onItemName": function (extra = {}, event) {
        event.preventDefault();
        let element = event.currentTarget;

        if (event.altKey) {
            let itemId = _getItemId(event);
            let item = this.actor.getOwnedItem(itemId);
            let field = element.dataset.actionFavouredName;

            let value = element.dataset.actionFavouredValue === "true";
            return item.update({[field]: !value});
        } else {
            let automaticDifficultyRoll = !event.shiftKey;
            Dice.taskCheck({
                actor: this.actor,
                user: game.user,
                askForOptions: automaticDifficultyRoll,
                actionValue: element.dataset.actionValue,
                actionName: element.dataset.actionName,
                wearyRoll: this.actor.data.data.stateOfHealth.weary.value,
                modifier: _getModifier(element, extra),
                shadowServant: extra.shadowServant,
            });
        }
    },

    /**
     * Either update the a skill toggling the favoured state if you click with alt key.
     * Or roll against this skill if no other key is used.
     * @param extra
     * @param event
     * @returns {*}
     */
    "onSkillName": function (extra = {}, event) {
        event.preventDefault();
        let element = event.currentTarget;

        if (event.altKey) {
            let field = element.dataset.actionFavouredName;
            let value = element.dataset.actionFavouredValue === "true";
            return this.actor.update({[field]: !value});
        } else {
            let automaticDifficultyRoll = !event.shiftKey;
            Dice.taskCheck({
                actor: this.actor,
                user: game.user,
                actionValue: element.dataset.actionValue,
                actionName: element.dataset.actionName,
                askForOptions: automaticDifficultyRoll,
                wearyRoll: this.actor.data.data.stateOfHealth.weary.value,
                modifier: _getModifier(element, extra),
                shadowServant: extra.shadowServant,
            });
        }
    },

    /**
     * Update the value of a stat when you change the value in the input text box.
     * Should be used for example when an item is in a sheet because the object doesn't belong to the proper stat of the sheet.
     * @param extra
     * @param event
     * @returns {*}
     */
    "onItemSkillModify": function (extra = {}, event) {
        event.preventDefault();

        if (event.altKey && event.shiftKey) {
            let element = event.currentTarget;
            let itemId = _getItemId(event);
            let item = this.actor.getOwnedItem(itemId);
            let field = element.dataset.field;
            let value = element.dataset.actionValue ?  parseInt(element.dataset.actionValue) : 0;
            return item.update({[field]: (value - 1 >= 0) ? value - 1 : value});
        } else if (event.altKey) {
            let element = event.currentTarget;
            let itemId = _getItemId(event);
            let item = this.actor.getOwnedItem(itemId);
            let field = element.dataset.field;
            let value = element.dataset.actionValue ? parseInt(element.dataset.actionValue) : 0;
            let max = parseInt(element.dataset.actionMaxValue);
            return item.update({[field]: (value + 1 <= max) ? value + 1 : value});
        }
    },

    /**
     * Update the value of a stat when you change the value in the input text box.
     * Should be used for example when an item is in a sheet because the object doesn't belong to the proper stat of the sheet.
     * @param extra
     * @param event
     * @returns {*}
     */
    "onSkillModify": function (extra = {}, event) {
        event.preventDefault();

        if (event.altKey && event.shiftKey) {
            let element = event.currentTarget;
            let field = element.dataset.field;
            let value = parseInt(element.dataset.actionValue);
            return this.actor.update({[field]: (value - 1 >= 0) ? value - 1 : value});
        } else if (event.altKey) {
            let element = event.currentTarget;
            let field = element.dataset.field;
            let value = parseInt(element.dataset.actionValue);
            let max = parseInt(element.dataset.actionMaxValue);
            return this.actor.update({[field]: (value + 1 <= max) ? value + 1 : value});
        }
    },

    /**
     * Update the value of a stat when you change the value in the input text box.
     * Should be used for example when an item is in a sheet because the object doesn't belong to the proper stat of the sheet.
     * @param extra
     * @param event
     * @returns {*}
     */
    "onThreeStatesModify": function (extra = {}, event) {

        event.preventDefault();

        if (event.altKey && event.shiftKey) {
            let element = event.currentTarget;
            let field = element.dataset.field;
            let value = parseInt(element.dataset.actionValue);
            return this.actor.update({[field]: (value - 1 >= 0) ? value - 1 : 2});
        } else if (event.altKey) {
            let element = event.currentTarget;
            let field = element.dataset.field;
            let value = parseInt(element.dataset.actionValue);
            return this.actor.update({[field]: (value + 1 <= 2) ? value + 1 : 0});
        }
    },

    /**
     * Update the value of a stat when you change the value in the input text box.
     * Should be used for example when an item is in a sheet because the object doesn't belong to the proper stat of the sheet.
     * @param event
     * @returns {*}
     */
    "onSkillEdit": function (event) {
        event.preventDefault();

        let element = event.currentTarget;
        let itemId = _getItemId(event);
        let item = this.actor.getOwnedItem(itemId);
        let field = element.dataset.field;

        // if the element is a checkbox then we have to translate the state (chack) into a boolean
        if (element.type === "checkbox") {
            return item.update({[field]: element.checked === true});
        }
        return item.update({[field]: element.value});
    },
}