export class TORBaseDie extends Die {
    constructor(termData) {
        termData.faces = 6;
        super(termData);
    }

    /* -------------------------------------------- */

    /** @override */
    static DENOMINATION = 's';

    static COMMAND = `d${TORBaseDie.DENOMINATION}`;

    static IMG = "systems/tor1e/assets/images/chat/dice_icons/chat_s_6.png"

    /* -------------------------------------------- */
    /** @override */
    static getResultLabel(result) {
        return CONFIG.tor1e.STANDARD_RESULTS[result].label;
    }

    static getCssClassName(index, data) {
        return data.actionValue && index < data.actionValue ? "mandatory" : "optional"
    }

    /**
     *
     * @param results
     * @param data some data available as a context for the evaluation
     * @returns {*} the value of the die
     */
    static getResultValue(results, data) {
        if (data.masteryDiceAdded > 0) {
            return results
                .sort((a, b) => CONFIG.tor1e.STANDARD_RESULTS[b.result].order - CONFIG.tor1e.STANDARD_RESULTS[a.result].order)
                .slice(0, data.actionValue)
                .map(die =>
                    CONFIG.tor1e.STANDARD_RESULTS[die.result].result).reduce((a, b) => a + b, 0);
        } else {
            return results.map(die =>
                CONFIG.tor1e.STANDARD_RESULTS[die.result].result).reduce((a, b) => a + b, 0);
        }
    }
}

export class TORWearyBaseDie extends Die {
    constructor(termData) {
        termData.faces = 6;
        super(termData);
    }

    /* -------------------------------------------- */

    /** @override */
    static DENOMINATION = 'w';

    static COMMAND = `d${TORWearyBaseDie.DENOMINATION}`;

    static IMG = "systems/tor1e/assets/images/chat/dice_icons/chat_s_3_w.png"

    /* -------------------------------------------- */
    /** @override */
    static getResultLabel(result) {
        return CONFIG.tor1e.WEARY_RESULTS[result].label;
    }

    static getCssClassName(index, data) {
        return data.actionValue && index < data.actionValue ? "mandatory" : "optional"
    }

    /**
     *
     * @param results
     * @param data some data available as a context for the evaluation
     * @returns {*} the value of the die
     */
    static getResultValue(results, data) {
        if (data.masteryDiceAdded > 0) {
            return results
                .sort((a, b) => CONFIG.tor1e.WEARY_RESULTS[b.result].order - CONFIG.tor1e.WEARY_RESULTS[a.result].order)
                .slice(0, data.actionValue)
                .map(die =>
                    CONFIG.tor1e.WEARY_RESULTS[die.result].result).reduce((a, b) => a + b, 0);
        } else {
            return results.map(die =>
                CONFIG.tor1e.WEARY_RESULTS[die.result].result).reduce((a, b) => a + b, 0);
        }
    }
}

export class TORFeatBaseDie extends Die {
    constructor(termData) {
        termData.faces = 12;
        super(termData);
    }

    /* -------------------------------------------- */

    /** @override */
    static DENOMINATION = 'f';

    static COMMAND = `d${TORFeatBaseDie.DENOMINATION}`;

    static IMG = "systems/tor1e/assets/images/chat/dice_icons/chat_f_gandalf.png"

    /* -------------------------------------------- */
    /** @override */
    static getResultLabel(result) {
        return CONFIG.tor1e.FEAT_RESULTS[result].label;
    }

    static getCssClassName(index, data) {
        if (data.bestFeatDie) {
            return index <= 0 ? "mandatory" : "optional"
        } else {
            return index <= 0 ? "optional" : "mandatory"
        }
    }

    /**
     *
     * @param results
     * @param data some data available as a context for the evaluation
     * @returns {*} the value of the die
     */
    static getResultValue(results, data) {
        if (results.length === 1) {
            if (data.shadowServant) {
                return CONFIG.tor1e.FEAT_RESULTS[results[0].result].result;
            } else {
                return CONFIG.tor1e.FEAT_RESULTS[results[0].result].result;
            }
        } else {
            let orderedFeats;
            if (data.shadowServant) {
                orderedFeats = results
                    .sort((a, b) => CONFIG.tor1e.FEAT_RESULTS[b.result].adversaryOrder - CONFIG.tor1e.FEAT_RESULTS[a.result].adversaryOrder)
            } else {
                orderedFeats = results
                    .sort((a, b) => CONFIG.tor1e.FEAT_RESULTS[b.result].order - CONFIG.tor1e.FEAT_RESULTS[a.result].order)
            }
            if (data && data.bestFeatDie) {
                return CONFIG.tor1e.FEAT_RESULTS[orderedFeats[0].result].result;
            } else {
                return CONFIG.tor1e.FEAT_RESULTS[orderedFeats[1].result].result;
            }
        }
    }
}
