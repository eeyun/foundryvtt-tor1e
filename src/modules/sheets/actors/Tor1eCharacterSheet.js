import {tor1eUtilities} from "../../utilities.js";

export default class Tor1eCharacterSheet extends ActorSheet {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["tor1e", "sheet", "actor"],
            width: 800,
            height: 800,
            template: `${CONFIG.tor1e.properties.rootpath}/templates/sheets/actors/character-sheet.hbs`
        });
    }

    getData() {
        const data = super.getData();
        let constants = CONFIG.tor1e.constants;
        data.config = CONFIG.tor1e;

        data.backgroundImages = CONFIG.tor1e.backgroundImages["character"];

        data.specialities = tor1eUtilities.filtering.getItemsBy(data.items, constants.trait, constants.speciality);
        data.distinctiveFeatures = tor1eUtilities.filtering.getItemsBy(data.items, constants.trait, constants.distinctiveFeature);
        data.flaws = tor1eUtilities.filtering.getItemsBy(data.items, constants.trait, constants.flaw);
        data.weaponSkills = tor1eUtilities.filtering.getItemsBy(data.items, constants.skill, constants.combat);
        data.rewards = tor1eUtilities.filtering.getItemsBy(data.items, constants.reward);
        data.virtues = tor1eUtilities.filtering.getItemsBy(data.items, constants.virtues);
        data.armours = tor1eUtilities.filtering.getItemsBy(data.items, constants.armour);
        data.weapons = tor1eUtilities.filtering.getItemsBy(data.items, constants.weapon);

        [data.leftMiscItem, data.rightMiscItem] =
            tor1eUtilities.filtering.getAndSplitItemsBy(data.items, constants.miscellaneous);

        data.headgear = tor1eUtilities.filtering.getItemBy(data.items, constants.armour, constants.headgear, true);
        data.shield = tor1eUtilities.filtering.getItemBy(data.items, constants.armour, constants.shield, true);
        data.armour = tor1eUtilities.filtering.getItemIn(data.items, constants.armour, [constants.mailArmour, constants.leatherArmour], true);

        data.threeStateWeary = data.config.threeStatesCheckbox[data.data.stateOfHealth.weary.value];
        data.threeStateWounded = data.config.threeStatesCheckbox[data.data.stateOfHealth.wounded.value];
        data.threeStatePoisoned = data.config.threeStatesCheckbox[data.data.stateOfHealth.poisoned.value];
        data.threeStateMiserable = data.config.threeStatesCheckbox[data.data.stateOfHealth.miserable.value];

        return data;
    }

    activateListeners(html) {
        /*
            code pattern
            html.find(cssSelector).event(this._someCallBack.bind(this));
         */
        html.find(".item-delete").click(tor1eUtilities.eventsProcessing.onItemDelete.bind(this));
        html.find(".item-edit").click(tor1eUtilities.eventsProcessing.onItemEdit.bind(this));
        html.find(".inline-edit").change(tor1eUtilities.eventsProcessing.onSkillEdit.bind(this));
        html.find(".toggle").click(tor1eUtilities.eventsProcessing.onToggle.bind(this));
        html.find(".editor-toggle").click(tor1eUtilities.eventsProcessing.onEditorToggle.bind(this));

        // Owner-only listeners
        if (this.actor.owner) {
            html.find(".inline-3-states-modify").click(tor1eUtilities.eventsProcessing.onThreeStatesModify.bind(this, {}));
            html.find(".inline-skill-modify").click(tor1eUtilities.eventsProcessing.onSkillModify.bind(this, {}));
            html.find(".inline-item-skill-modify").click(tor1eUtilities.eventsProcessing.onItemSkillModify.bind(this, {}));

            html.find(".skill-name").click(tor1eUtilities.eventsProcessing.onSkillName.bind(this, {}));
            html.find(".stat-name").click(tor1eUtilities.eventsProcessing.onStatName.bind(this, {}));
            html.find(".item-name").click(tor1eUtilities.eventsProcessing.onItemName.bind(this, {}));
        }

        super.activateListeners(html);
    }

}