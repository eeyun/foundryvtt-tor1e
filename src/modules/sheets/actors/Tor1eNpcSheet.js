import {tor1eUtilities} from "../../utilities.js";

export default class Tor1eNpcSheet extends ActorSheet {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["tor1e", "sheet", "actor"],
            width: 500,
            height: 725,
            template: `${CONFIG.tor1e.properties.rootpath}/templates/sheets/actors/nonplayercharacter-sheet.hbs`
        });
    }

    getData() {
        const data = super.getData();
        let constants = CONFIG.tor1e.constants;
        data.config = CONFIG.tor1e;

        data.backgroundImages = CONFIG.tor1e.backgroundImages["npc"];

        data.specialities = tor1eUtilities.filtering.getItemsBy(data.items, constants.trait, constants.speciality);
        data.distinctiveFeatures = tor1eUtilities.filtering.getItemsBy(data.items, constants.trait, constants.distinctiveFeature);
        data.flaws = tor1eUtilities.filtering.getItemsBy(data.items, constants.trait, constants.flaw);
        data.skills = tor1eUtilities.filtering.getItemsNot(data.items, constants.skill, constants.combat);
        data.weaponSkills = tor1eUtilities.filtering.getItemsBy(data.items, constants.skill, constants.combat);

        return data;
    }

    activateListeners(html) {
        /*
            code pattern
            html.find(cssSelector).event(this._someCallBack.bind(this));
         */
        html.find(".item-delete").click(tor1eUtilities.eventsProcessing.onItemDelete.bind(this));
        html.find(".item-edit").click(tor1eUtilities.eventsProcessing.onItemEdit.bind(this));
        html.find(".inline-edit").change(tor1eUtilities.eventsProcessing.onSkillEdit.bind(this));
        html.find(".toggle").click(tor1eUtilities.eventsProcessing.onToggle.bind(this));
        html.find(".editor-toggle").click(tor1eUtilities.eventsProcessing.onEditorToggle.bind(this));

        // Owner-only listeners
        if (this.actor.owner) {
            let extra = {
                "favouredSkillValue": this.actor.data.data.attributeLevel.value
            }
            html.find(".inline-item-skill-modify").click(tor1eUtilities.eventsProcessing.onItemSkillModify.bind(this, {}));
            html.find(".item-name").click(tor1eUtilities.eventsProcessing.onItemName.bind(this, extra));
        }

        super.activateListeners(html);
    }

}