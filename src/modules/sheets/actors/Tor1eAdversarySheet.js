import {tor1eUtilities} from "../../utilities.js";

export default class Tor1eAdversarySheet extends ActorSheet {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["tor1e", "sheet", "actor"],
            width: 550,
            height: 775,
            template: `${CONFIG.tor1e.properties.rootpath}/templates/sheets/actors/adversary-sheet.hbs`
        });
    }

    getData() {
        const data = super.getData();
        let constants = CONFIG.tor1e.constants;
        data.config = CONFIG.tor1e;

        data.backgroundImages = CONFIG.tor1e.backgroundImages["adversary"];

        [data.leftSpecialAbilities, data.rightSpecialAbilities] =
            tor1eUtilities.filtering.getAndSplitItemsBy(data.items, constants.specialAbility);

        data.weapons = tor1eUtilities.filtering.getItemsBy(data.items, constants.weapon);
        data.headgear = tor1eUtilities.filtering.getItemBy(data.items, constants.armour, constants.headgear);
        data.shield = tor1eUtilities.filtering.getItemBy(data.items, constants.armour, constants.shield);
        data.armour = tor1eUtilities.filtering.getItemIn(data.items, constants.armour, [constants.mailArmour, constants.leatherArmour]);

        return data;
    }

    activateListeners(html) {
        /*
            code pattern
            html.find(cssSelector).event(this._someCallBack.bind(this));
         */
        html.find(".item-delete").click(tor1eUtilities.eventsProcessing.onItemDelete.bind(this));
        html.find(".item-edit").click(tor1eUtilities.eventsProcessing.onItemEdit.bind(this));
        html.find(".inline-edit").change(tor1eUtilities.eventsProcessing.onSkillEdit.bind(this));
        html.find(".toggle").click(tor1eUtilities.eventsProcessing.onToggle.bind(this));
        html.find(".editor-toggle").click(tor1eUtilities.eventsProcessing.onEditorToggle.bind(this));


        // Owner-only listeners
        if (this.actor.owner) {
            let extra = {
                "favouredSkillValue": this.actor.data.data.attributeLevel.value,
                "shadowServant": true,
            }
            html.find(".inline-skill-modify").click(tor1eUtilities.eventsProcessing.onSkillModify.bind(this, {}));
            html.find(".inline-item-skill-modify").click(tor1eUtilities.eventsProcessing.onItemSkillModify.bind(this, {}));
            html.find(".skill-name").click(tor1eUtilities.eventsProcessing.onSkillName.bind(this, extra));
            html.find(".item-name").click(tor1eUtilities.eventsProcessing.onItemName.bind(this, extra));
        }
        super.activateListeners(html);
    }

}