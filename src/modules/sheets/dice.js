import * as Tor1eDie from "../die.js"
import {Tor1eRoll} from "../Tor1eRoll.js";

async function getTaskCheckOptions(taskType, difficulty, formula) {
    const template = `${CONFIG.tor1e.properties.rootpath}/templates/chat/task-check-dialog.hbs`;
    let backgroundImages = CONFIG.tor1e.backgroundImages["dice-roll"];
    const html = await renderTemplate(template, {
        difficulty: difficulty,
        featDice: false,
        bonusDie: false,
        masteryDice: 0,
        backgroundImages: backgroundImages,
        formula: formula
    });

    return new Promise(resolve => {
        const data = {
            title: game.i18n.format("tor1e.chat.taskCheck.title", {type: taskType}),
            content: html,
            buttons: {
                normal: {
                    label: game.i18n.localize("tor1e.chat.actions.roll"),
                    callback: html => resolve(_processTaskCheckOptions(html[0].querySelector("form")))
                },
                cancel: {
                    label: game.i18n.localize("tor1e.chat.actions.cancel"),
                    callback: html => resolve({cancelled: true})
                }
            },
            default: "normal",
            close: () => resolve({cancelled: true})

        }
        const options = {
            width: 450,
            height: 350,
        }
        new Dialog(data, options).render(true)
    });
}

function _processTaskCheckOptions(form) {
    return {
        difficulty: parseInt(form.difficulty.value),
        featDiceAdded: form.featDice.checked ? 1 : 0,
        bestFeatDie: !form.bestFeatDie.checked,
        bonusDiceAdded: form.bonusDie.checked ? 1 : 0,
        masteryDiceAdded: parseInt(form.masteryDice.value),
    }
}

export async function taskCheck(
    {
        actor = null,
        user = null,
        actionValue = null,
        actionName = "",
        difficulty = 14,
        featDiceValue = 1,
        bonusDiceAdded = 0,
        masteryDiceAdded = 0,
        featDiceAdded = 0,
        bestFeatDie = true,
        wearyRoll = false,
        shadowServant = false,
        askForOptions = true,
        taskType = "Standard",
        modifier = 0
    } = {}) {

    let nbDicePonderated = actionValue;

    if (askForOptions) {
        let nbSuccessDice = parseInt(actionValue);
        let formula = {
            feat: {
                img: Tor1eDie.TORFeatBaseDie.IMG,
                value: featDiceValue
            },
            success: {
                display: nbSuccessDice > 0,
                img: wearyRoll ? Tor1eDie.TORWearyBaseDie.IMG : Tor1eDie.TORBaseDie.IMG,
                value: nbSuccessDice
            },
            modifier: {
                display: modifier && modifier !== 0,
                value: modifier
            }
        }

        let checkOptions = await getTaskCheckOptions(taskType, difficulty, formula);

        if (checkOptions.cancelled) {
            return;
        }

        difficulty = checkOptions.difficulty;
        bonusDiceAdded = checkOptions.bonusDiceAdded;
        masteryDiceAdded = checkOptions.masteryDiceAdded;
        featDiceAdded = checkOptions.featDiceAdded;
        bestFeatDie = checkOptions.bestFeatDie;
    }

    Object.defineProperty(String.prototype, "sanityze", {
        value: function sanityze() {
            return this.replace(/^\r?\n|\r/, "");
        },
        writable: true,
        configurable: true
    });

    function _buildRollFormula() {
        let nbDiceBase = parseInt(actionValue);
        nbDicePonderated = nbDiceBase + bonusDiceAdded > 6 ? 6 : nbDiceBase + bonusDiceAdded;
        let nbSuccessDice = nbDicePonderated + masteryDiceAdded;
        let nbFeatDice = featDiceValue + featDiceAdded;
        let bonus = (modifier !== 0) ? ` + ${modifier}` : ""
        let baseDice = wearyRoll ? wearyDie : standardDie;
        let successDice = nbSuccessDice > 0 ? ` + (${nbSuccessDice})${baseDice} ` : ""
        return `(${nbFeatDice})${featDie}${successDice}${bonus}`;
    }

    let standardDie = Tor1eDie.TORBaseDie.COMMAND;
    let wearyDie = Tor1eDie.TORWearyBaseDie.COMMAND;
    let featDie = Tor1eDie.TORFeatBaseDie.COMMAND;


    let rollFormula = _buildRollFormula();
    let rollData = {
        actionValue: nbDicePonderated,
        masteryDiceAdded: masteryDiceAdded,
        difficulty: difficulty,
        modifier: modifier,
        bestFeatDie: bestFeatDie,
        flavor: {
            user: user._id,
            action: actionName,
            owner: {
                id: actor.id,
                img: actor.img,
                name: actor.name
            }
        },
        shadowServant: shadowServant
    };

    let rollResult = new Tor1eRoll(rollFormula, rollData).roll();

    let messageData = {
        speaker: ChatMessage.getSpeaker(),

    }

    rollResult.toMessage(messageData);

    return rollResult;
}