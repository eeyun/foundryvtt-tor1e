export default class Tor1eItemSheet extends ItemSheet {

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["tor1e", "sheet", "item"],
            width: 450,
            height: 550
        });
    }

    get template() {
        return `systems/tor1e/templates/sheets/items/${this.item.data.type}-sheet.hbs`
    }

    getData() {
        const data = super.getData();
        data.config = CONFIG.tor1e;
        data.backgroundImages = CONFIG.tor1e.backgroundImages[`${this.item.data.type}`];
        return data;
    }
}